import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        time: '0',
        selectedIcon: '',
        arSelectedIcons: [],
        arLiked: [],
    },
    actions: {
        addIconAction({commit}, icon) {
            commit('addIcon', icon)
        },
        addSelectedIconAction({commit}, icon) {
            commit('addSelectedIcon', icon)
        },
        removeSelectedIconAction({commit}) {
            commit('removeSelectedIcon')
        },
        setTimeAction({commit}, time) {
            commit('setTime', time)
        },
        addLikeAction({commit}, index) {
            commit('addLike', index)
        }
    },
    mutations: {
        addIcon(state, icon) {
            state.selectedIcon = icon
        },
        addSelectedIcon(state, icon) {
            state.arSelectedIcons.push(icon)
        },
        removeSelectedIcon(state) {
            state.arSelectedIcons = []
        },
        setTime(state, time) {
            state.time = time
        },
        addLike(state, index) {
            if (state.arLiked.includes(index)) {
                for (let i = 0; i < state.arLiked.length; i++) {
                    if(state.arLiked[i] === index) {
                        state.arLiked.splice(i,1);
                    }
                }
            } else {
                state.arLiked.push(index)
            }
        }
    }
})

import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faTrash,
    faPen,
    faSave,
    faTimes,
    faPlus,
    faChevronUp,
    faChevronDown,
    faFileCsv,
    faAd,
    faRuler,
    faAnchor,
    faFile,
    faAtom,
    faBaby,
    faBook,
    faCaretLeft,
    faHeart
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faTrash, faPen, faSave, faTimes, faPlus, faChevronUp, faChevronDown, faFileCsv, faAd, faRuler, faAnchor, faFile, faAtom, faBaby, faBook, faCaretLeft, faHeart)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
    store,
    render: function (h) {
        return h(App)
    },
}).$mount('#app')
